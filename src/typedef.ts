export type Dictionary = {
    headers: {
        list: string,
        category: string,
        range: string,
        cart: string,
        total: string
    },
    categories: {
        all: string,
        jeans: string,
        shirts: string,
        t_shirts: string
    }
}

export type Lang = 'en' | 'ua';

