import React, {Component} from 'react';
import './App.css';

import {LocalLang} from "./components/ui-components/local-lang/local-lang";

export default class App extends Component {
    render() {
        return (
              <LocalLang/>
        )
    }
}
