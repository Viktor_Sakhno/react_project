import React, {Component} from 'react';
import {Product} from './ShoppingList'
import Filter from "../ui-components/filter/Filter";
import {LocalContext} from '../ui-components/local-lang/context';
import {PageSizeProduct} from '../ui-components/pagination/pageSizeProduct';


type State = {
    addToCart: (id: number) => void,
    minPrice: number,
    maxPrice: number,
    filterCategory: string,
    filterMinPrice: number,
    filterMaxPrice: number,
    filtered: Product[]
}

type Props = {
    items: Product[],
    addToCart: (id: number) => void
}

export default class ProductsList extends Component<Props, State> {
    state = {
        filtered: this.props.items,
        addToCart: this.props.addToCart,
        minPrice: 0,
        maxPrice: 0,
        filterCategory: 'all',
        filterMinPrice: 0,
        filterMaxPrice: 0
    };

    componentDidMount(): void {
        const items = this.state.filtered;
        const minPrice: number = items.reduce((acc: number, item: Product) => Math.min(acc, item.price), items[0].price);
        const maxPrice: number = items.reduce((acc: number, item: Product) => Math.max(acc, item.price), 0);
        this.setState({
            minPrice,
            maxPrice,
            filterCategory: 'all',
            filterMinPrice: minPrice,
            filterMaxPrice: maxPrice
        })
    }

    filterHandler(event: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        const {name, value} = event.target;
        let filtered = this.props.items;

        this.setState((prev) => {
            let newState = {...prev, [name]: value};

            const {filterCategory, filterMinPrice, filterMaxPrice} = newState;

            if (filterCategory !== 'all') {
                filtered = filtered.filter(item => item.category === filterCategory);
            }
            newState.minPrice = filtered.reduce((acc: number, item: Product) => Math.min(acc, item.price), filtered[0].price);
            newState.maxPrice = filtered.reduce((acc: number, item: Product) => Math.max(acc, item.price), 0);

            filtered = filtered.filter((item) => (item.price <= filterMaxPrice && item.price >= filterMinPrice));

            newState.filtered = filtered;
            return newState;
        });
    }

    static contextType = LocalContext;

    render() {
        let {filtered, minPrice, maxPrice, addToCart, filterCategory, filterMinPrice, filterMaxPrice} = this.state;
        return (
            <div className={"product-list"}>
                <h1>{this.context.dictionary.headers.list}</h1>
                <Filter
                    minPrice={minPrice}
                    maxPrice={maxPrice}
                    filterCategory={filterCategory}
                    filterMinPrice={filterMinPrice}
                    filterMaxPrice={filterMaxPrice}
                    filterHandler={(event) => this.filterHandler(event)}/>

                <div>
                    <PageSizeProduct data={filtered} addToCart={addToCart}/>
                </div>

            </div>
        )
    }
};
