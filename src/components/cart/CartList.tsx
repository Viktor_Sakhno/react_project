import React from 'react';
import {CartItem} from './ShoppingList'
import {LocalContext} from '../ui-components/local-lang/context';
import {PageSizeCart} from '../ui-components/pagination/pageSizeCart';



export type Props = {
    items: CartItem[],
    remove: (id:number) => void,
    plusQuantity: (id:number) => void,
    minusQuantity: (id:number) => void,
}

const CartList = (props: Props) => {
    const {items, remove, plusQuantity, minusQuantity} = props;
    const total = items.reduce((acc, item) => (acc + (item.price * item.quantity)), 0);
    return (
        <LocalContext.Consumer>
            {(context) => {
                return context && (
                    <>
                        <div className={"cart-list"}>
                            {items.length ? <h1>{context.dictionary.headers.cart}</h1> : null}
                            <PageSizeCart items={items} remove={remove} plusQuantity={plusQuantity} minusQuantity={minusQuantity}/>
                            {items.length ?
                                <div className={"cart-list__total-amount"}>{context.dictionary.headers.total}: {total}</div> : null}
                        </div>
                    </>)
            }}
        </LocalContext.Consumer>
    )
};

export default CartList;

