import React, {Component} from 'react';
import './ShoppingList.css';

import ProductsList from "./ProductsList";
import CartList from "./CartList";
import {LangSwitcher} from "../ui-components/local-lang/lang-switcher"
import {LocalContext} from '../ui-components/local-lang/context';


export type Product = {
    id: number,
    title: string,
    price: number,
    category: string,
    images: string[]
}

export type CartItem = Product & {
    quantity: number
}

type State =  {
    productsList: Product[],
    cart: CartItem[]
}

export default class ShoppingList extends Component<{}, State> {
    state = {
        productsList: [
            {
                id: 1,
                title: "Джинси Levi's",
                price: 130,
                category: 'Джинси',
                images: ["https://i1.rozetka.ua/goods/13231683/levis_28833_0440_34_34_images_13231683600.jpg",
                    "https://i1.rozetka.ua/goods/13231684/levis_28833_0440_34_34_images_13231684200.jpg",
                    "https://i2.rozetka.ua/goods/13231684/levis_28833_0440_34_34_images_13231684728.jpg"
                ]
            },
            {
                id: 2,
                title: "Джинси H&M",
                price: 150,
                category: 'Джинси',
                images: ["https://i1.rozetka.ua/goods/13917867/hm_2000000044620_images_13917867070.jpg",
                    "https://i1.rozetka.ua/goods/13917867/hm_2000000044620_images_13917867628.jpg",
                    "https://i2.rozetka.ua/goods/13917868/hm_2000000044620_images_13917868120.jpg"
                ]
            },
            {
                id: 3,
                title: "Джинси Colin’s",
                price: 120,
                category: 'Джинси',
                images: ["https://i1.rozetka.ua/goods/13590515/colins_8681597611422_images_13590515161.jpg",
                    "https://i2.rozetka.ua/goods/13590515/colins_8681597611422_images_13590515605.jpg",
                    "https://i2.rozetka.ua/goods/13590516/colins_8681597611422_images_13590516523.jpg"
                ]
            },
            {
                id: 4,
                title: "Сорочка ISSA PLUS",
                price: 100,
                category: 'Сорочки',
                images: ["https://i2.rozetka.ua/goods/12786072/issa_plus_2000040985143_images_12786072103.jpg",
                    "https://i1.rozetka.ua/goods/12786072/issa_plus_2000040985143_images_12786072763.jpg",
                    "https://i1.rozetka.ua/goods/12786073/issa_plus_2000040985143_images_12786073375.jpg"
                ]
            },
            {
                id: 5,
                title: "Сорочка Piazza Italia",
                price: 90,
                category: 'Сорочки',
                images: ["https://i2.rozetka.ua/goods/14180365/piazza_italia_2018067005058_images_14180365538.jpg",
                    "https://i1.rozetka.ua/goods/14180366/piazza_italia_2018067005058_images_14180366182.jpg",
                    "https://i2.rozetka.ua/goods/14180366/piazza_italia_2018067005058_images_14180366882.jpg"
                ]
            },
            {
                id: 6,
                title: "Сорочка Leonardo Savelli",
                price: 110,
                category: 'Сорочки',
                images: ["https://i2.rozetka.ua/goods/11139611/leonardo_savelli_1489847182_images_11139611043.jpg",
                    "https://i1.rozetka.ua/goods/11139610/leonardo_savelli_1489847182_images_11139610719.jpg",
                    "https://i2.rozetka.ua/goods/11139611/leonardo_savelli_1489847182_images_11139611769.jpg"
                ]
            },
            {
                id: 7,
                title: "Футболка Puma",
                price: 80,
                category: 'Футболки',
                images: ["https://i2.rozetka.ua/goods/10913163/puma_4059506775031_images_10913163609.jpg",
                    "https://i1.rozetka.ua/goods/10885339/puma_4059506775031_images_10885339377.jpg",
                    "https://i1.rozetka.ua/goods/10885340/puma_4059506775031_images_10885340535.jpg"
                ]
            },
            {
                id: 8,
                title: "Футболка Gildan",
                price: 60,
                category: 'Футболки',
                images: ["https://i2.rozetka.ua/goods/10882720/gildan_5000000087863_images_10882720851.jpg",
                    "https://i1.rozetka.ua/goods/10882721/gildan_5000000087863_images_10882721859.jpg"
                ]
            },
            {
                id: 9,
                title: "Футболка Sol's",
                price: 70,
                category: 'Футболки',
                images: ["https://i1.rozetka.ua/goods/3039830/sols_victory_11150102m_images_3039830863.jpg",
                    "https://i2.rozetka.ua/goods/3039831/sols_victory_11150102m_images_3039831775.jpg"
                ]
            },
            {
                id: 10,
                title: "Футболка Armani",
                price: 50,
                category: 'Футболки',
                images: ["https://i1.rozetka.ua/goods/11349502/34565847_images_11349502716.jpg",
                    "https://i2.rozetka.ua/goods/11349502/34565847_images_11349502914.jpg",
                    "https://i2.rozetka.ua/goods/11349503/34565847_images_11349503184.jpg"
                ]
            }
        ],
        cart: []
    };

    addToCart = (id: number) => {
        let cart: CartItem[] = this.state.cart;
        let {productsList} = this.state;
        const productInCart = cart.find(product => product.id === id);

        if (productInCart) {
            return;
        } else {
            let productToAdd = productsList.find(product => product.id === id);

            if (productToAdd) {
                const newCart = [...cart, {...productToAdd, quantity: 1}];

                this.setState({
                    cart: newCart
                });

            }
        }
    };

    remove = (id: number) => {
        const cart = this.state.cart.filter((item: CartItem) => item.id !== id);
        this.setState({cart});
    };

    plusQuantity = (id: number) => {
        let cart: CartItem[] = this.state.cart;
        const productInCart = cart.find(product => product.id === id);

        if (productInCart) {
            productInCart.quantity += 1;
            this.setState({cart});
        }
    };

    minusQuantity = (id: number) => {
        let cart: CartItem[] = this.state.cart;
        const productInCart = cart.find(product => product.id === id);

        if (productInCart) {
            if (productInCart.quantity > 1) {
                productInCart.quantity -= 1;
                this.setState({cart});
            }
        }
    };
    static contextType = LocalContext;

    render() {
        const {productsList, cart} = this.state;

        return (
            <div>
                <LangSwitcher lang={this.context.lang} changeLang={this.context.changeLang}/>
                <div className={"shopList"}>
                    <ProductsList
                        addToCart={this.addToCart}
                        items={productsList}
                    />
                    <CartList
                        remove={this.remove}
                        plusQuantity={this.plusQuantity}
                        minusQuantity={this.minusQuantity}
                        items={cart}/>
                </div>
            </div>
        )
    }
}
