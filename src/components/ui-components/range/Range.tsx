import React from "react";
import './Range.css'

type Props = {
    min: number,
    max: number,
    value: number,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
    name: string,
}

export default (props: Props) => {
    const {name, min, max, value, onChange} = props;
    return (
        <div className={'range-component'} >
            <input className="display-value" type="text" disabled={true} value={value}/>
            <input
                className='range-input'
                type="range"
                name={name}
                min={min}
                max={max}
                value={value}
                onChange={onChange}/>
        </div>
    )
};
