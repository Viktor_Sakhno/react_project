import React, {Component} from "react";
import "./Slider.css"


type State = {
    images: string[],
    currentImageIndex: number,
    canGoPrev: boolean,
    canGoNext: boolean

}

type Props = {
    images: string[]
}

export default class Slider extends Component<Props, State> {


    state = {
        images: this.props.images,
        currentImageIndex: 0,
        canGoPrev: false,
        canGoNext: true

    };


    nextSliderHandler = (e: React.SyntheticEvent<HTMLButtonElement>) => {
        let newIndex = this.state.currentImageIndex;
        if (e.currentTarget.dataset.direction === 'next') {
            if (newIndex < this.state.images.length - 1) {
                newIndex = this.state.currentImageIndex + 1;
                this.setState({canGoPrev: true});
            }

            if (newIndex === this.state.images.length - 1) {
                this.setState({canGoNext: false});
            }
        } else {
            if (newIndex > 0) {
                newIndex = this.state.currentImageIndex - 1;
                this.setState({canGoNext: true});
            }

            if (newIndex === 0) {
                this.setState({canGoPrev: false});
            }
        }
        this.setState({currentImageIndex: newIndex});
    };


    render() {
        return (
            <div className={'slider'}>
                <div className={'slider__image-box'}>
                    <img className={'slider__image'} src={this.state.images[this.state.currentImageIndex]} alt={""}/>

                </div>
                <div className={'slider__buttons'}>
                    <button disabled={!this.state.canGoPrev} data-direction="prev"
                            onClick={this.nextSliderHandler}>PREV
                    </button>
                    <button disabled={!this.state.canGoNext} data-direction="next"
                            onClick={this.nextSliderHandler}>Next
                    </button>

                </div>

            </div>
        );
    }
}
