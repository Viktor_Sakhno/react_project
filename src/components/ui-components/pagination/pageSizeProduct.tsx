import React, {FC} from 'react';
import {usePagination} from './usePagination';
import {Product} from '../../cart/ShoppingList';
import Slider from '../slider/Slider';

type Props = {
    data: Product[],
    addToCart: (id: number) => void
};


export const PageSizeProduct: FC<Props> = props => {
    const {pageData, pagination} = usePagination<Product>(props.data, 3);

    return (
        <div>
            <div className='pagination-controls'>
            {pagination}
            </div>
            <div className={'product-list__field'}>
                {pageData && pageData.map(item => (<div key={item.id} className={"product-card"}>
                    <Slider images={item.images}/>
                    <div className={"product-card__title"}>{item.title}</div>
                    <div className={"product-card__price"}>price: {item.price}</div>
                    <div className={"product-card__btn"}>
                        <button onClick={() => {
                            props.addToCart(item.id)
                        }}>Buy
                        </button>
                    </div>
                </div>))}
            </div>


        </div>
    )
};
