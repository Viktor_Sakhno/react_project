import React, {FC} from 'react';
import {usePagination} from './usePagination';
import {CartItem} from '../../cart/ShoppingList';
import Slider from '../slider/Slider';
import {Props} from '../../cart/CartList';



export const PageSizeCart: FC<Props> = props => {
    const {pageData, pagination} = usePagination<CartItem>(props.items, 2);

    return (
        <div>
            <div className='pagination-controls'>
                {pagination}
            </div>
            <div className={'product-list__field'}>
                {pageData && pageData.map((item) => <div key={item.id} className={"product-card"}>
                    <Slider images={item.images}/>
                    <div className={"product-card__title"}>{item.title}</div>
                    <div className={"product-card__price"}>price: {item.price}</div>
                    <div className={"counter-buttons"}>quantity: {item.quantity}
                        <button className={'counter-buttons__btn'} onClick={() => {
                            props.minusQuantity(item.id)
                        }}>-
                        </button>
                        <button className={'counter-buttons__btn'} onClick={() => {
                            props.plusQuantity(item.id)
                        }}>+
                        </button>
                    </div>
                    <div className={"product-card__amount"}>amount: {item.price * item.quantity}</div>
                    <div className={"remove-buttons"}>
                        <button className={'remove-buttons__btn'} onClick={() => {
                            props.remove(item.id)
                        }}>delete
                        </button>
                    </div>
                </div>)}
            </div>
        </div>
    );
};
