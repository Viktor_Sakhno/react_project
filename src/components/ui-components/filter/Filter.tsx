import React from "react";
import Range from "../range/Range";
import './Filter.css'
import {LocalContext} from '../local-lang/context';


type Props = {
    minPrice: number,
    maxPrice: number,
    filterCategory: string,
    filterMinPrice: number
    filterMaxPrice: number
    filterHandler: (event: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) => void
}

const Filter = (props: Props) => {
    const {filterCategory, filterMinPrice, filterMaxPrice, filterHandler, minPrice, maxPrice} = props;

    return (
        <LocalContext.Consumer>
            {(context) => {
                return context && (
                    <>
                        <div className="filter-box">
                            <div className={'filter'}>
                                <h2>{context.dictionary.headers.category}</h2>
                                <select className="filter__item" name="filterCategory" onChange={filterHandler}
                                        value={filterCategory}>
                                    <option value="all">{context.dictionary.categories.all}</option>
                                    <option value="Джинси">{context.dictionary.categories.jeans}</option>
                                    <option value="Сорочки">{context.dictionary.categories.shirts}</option>
                                    <option value="Футболки">{context.dictionary.categories.t_shirts}</option>
                                </select>
                            </div>
                            <div className={'range'}>
                                <h2>{context.dictionary.headers.range}</h2>
                                <div className={'range__item'}>
                                    <Range
                                        name="filterMinPrice"
                                        min={minPrice}
                                        max={maxPrice}
                                        value={filterMinPrice}
                                        onChange={filterHandler}/>
                                    <Range
                                        name="filterMaxPrice"
                                        min={minPrice}
                                        max={maxPrice}
                                        value={filterMaxPrice}
                                        onChange={filterHandler}/>
                                </div>
                            </div>
                        </div>
                    </>)
            }}
        </LocalContext.Consumer>
    )
};

export default Filter;
