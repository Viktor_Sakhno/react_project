import {createContext} from 'react';
import {Dictionary, Lang} from '../../../typedef';

export type Context = {
    dictionary: Dictionary,
    changeLang: (lang: Lang) => void,
    lang: Lang
}

export const LocalContext = createContext<Context | null>(null);
