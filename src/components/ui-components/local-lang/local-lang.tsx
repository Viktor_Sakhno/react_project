import React, {Component} from 'react';
import ShoppingList from '../../cart/ShoppingList';
import {Dictionary, Lang} from '../../../typedef';
import {LocalContext} from './context';

type Props = {};

type State = {
    lang: Lang,
    dictionary?: Dictionary
}

export class LocalLang extends Component<Props, State> {
    state: State = {
        lang: 'en',
    };

    loadDictionary(lang: Lang) {
        fetch(`/i18n/${lang}.json`)
            .then(result => result.json())
            .then((data) => {
                this.setState({dictionary: data});
            });
    }

    componentDidMount() {
        this.loadDictionary(this.state.lang);
    }

    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>,) {
        if (this.state.lang !== prevState.lang) {
            this.loadDictionary(this.state.lang);
        }
    }


    render() {
        if (!this.state.dictionary) {
            return null;
        }

        const contextValue = {
            dictionary: this.state.dictionary,
            changeLang: (lang: Lang) => this.setState({lang}),
            lang: this.state.lang
        };
        return (
            <LocalContext.Provider value={contextValue}>
                    <ShoppingList/>
            </LocalContext.Provider>
        )
    }
}
