import React, {FC} from 'react';
import {Lang} from '../../../typedef';


type Props = {
    lang: Lang,
    changeLang: (lang: Lang) => void
};

export const LangSwitcher: FC<Props> = props => {
    const createHandler = (lang: Lang) => () => {
        props.changeLang(lang)
    };

    return (
        <>
            <button onClick={createHandler('en')}>en</button>
            <button onClick={createHandler('ua')}>ua</button>
        </>
    )
};
